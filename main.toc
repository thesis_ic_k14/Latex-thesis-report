\contentsline {chapter}{\numberline {1}Gi\IeC {\'\ohorn }i thi\IeC {\d \ecircumflex }u}{1}
\contentsline {chapter}{\numberline {2}C\IeC {\'a}c c\IeC {\^o}ng tr\IeC {\`\i }nh li\IeC {\^e}n quan}{3}
\contentsline {section}{\numberline {2.1}L\IeC {\d i}ch s\IeC {\h \uhorn } ph\IeC {\'a}t tri\IeC {\h \ecircumflex }n m\IeC {\^o} h\IeC {\`\i }nh}{3}
\contentsline {subsection}{\numberline {2.1.1}C\IeC {\'a}c c\IeC {\^o}ng tr\IeC {\`\i }nh nghi\IeC {\^e}n c\IeC {\'\uhorn }u tr\IeC {\^e}n th\IeC {\'\ecircumflex } gi\IeC {\'\ohorn }i}{3}
\contentsline {subsubsection}{Show and Tell: A Neural Image Caption Generator}{4}
\contentsline {subsubsection}{Show, Attend and Tell: Neural Image Caption Generation with Visual Attention}{5}
\contentsline {subsubsection}{Bottom-Up and Top-Down Attention cho vi\IeC {\d \ecircumflex }c ph\IeC {\'a}t sinh c\IeC {\^a}u m\IeC {\^o} t\IeC {\h a} \IeC {\dj }\IeC {\'\ocircumflex }i v\IeC {\'\ohorn }i h\IeC {\`\i }nh \IeC {\h a}nh}{6}
\contentsline {subsection}{\numberline {2.1.2}C\IeC {\'a}c c\IeC {\^o}ng tr\IeC {\`\i }nh nghi\IeC {\^e}n c\IeC {\'\uhorn }u \IeC {\h \ohorn } Vi\IeC {\d \ecircumflex }t Nam}{7}
\contentsline {section}{\numberline {2.2}C\IeC {\'a}c b\IeC {\d \ocircumflex } d\IeC {\~\uhorn } li\IeC {\d \ecircumflex }u chu\IeC {\h \acircumflex }n}{8}
\contentsline {section}{\numberline {2.3}C\IeC {\'a}c th\IeC {\uhorn }\IeC {\'\ohorn }c \IeC {\dj }o \IeC {\dj }\IeC {\'a}nh gi\IeC {\'a}}{9}
\contentsline {chapter}{\numberline {3}Ki\IeC {\'\ecircumflex }n th\IeC {\'\uhorn }c n\IeC {\`\ecircumflex }n t\IeC {\h a}ng}{10}
\contentsline {section}{\numberline {3.1}Neural Networks}{10}
\contentsline {subsection}{\numberline {3.1.1}Perceptron}{10}
\contentsline {subsection}{\numberline {3.1.2}Multi-Layer Perceptron}{11}
\contentsline {subsection}{\numberline {3.1.3}Backpropagation}{12}
\contentsline {subsection}{\numberline {3.1.4}Convolutional neural networks}{12}
\contentsline {subsection}{\numberline {3.1.5}Recurrent Neural Networks}{14}
\contentsline {subsubsection}{Long Short Term Memory networks}{16}
\contentsline {subsubsection}{Gated Recurrent Unit}{17}
\contentsline {section}{\numberline {3.2}ResNet}{18}
\contentsline {section}{\numberline {3.3}Faster RCNN}{19}
\contentsline {section}{\numberline {3.4}C\IeC {\ohorn } ch\IeC {\'\ecircumflex } attention}{23}
\contentsline {subsection}{\numberline {3.4.1}Attention trong b\IeC {\`a}i to\IeC {\'a}n Image Captioning}{24}
\contentsline {subsection}{\numberline {3.4.2}Deterministic "Soft" Attention}{24}
\contentsline {subsection}{\numberline {3.4.3}Stochastic "Hard" Attention}{25}
\contentsline {subsection}{\numberline {3.4.4}"Soft" Attention vs "Hard" Attention}{26}
\contentsline {chapter}{\numberline {4}H\IeC {\uhorn }\IeC {\'\ohorn }ng ti\IeC {\'\ecircumflex }p c\IeC {\d \acircumflex }n}{27}
\contentsline {section}{\numberline {4.1}M\IeC {\^o} h\IeC {\`\i }nh c\IeC {\ohorn } b\IeC {\h a}n}{27}
\contentsline {subsection}{\numberline {4.1.1}M\IeC {\^o} h\IeC {\`\i }nh tr\IeC {\'\i }ch xu\IeC {\'\acircumflex }t \IeC {\dj }\IeC {\d \abreve }c tr\IeC {\uhorn }ng t\IeC {\`\uhorn } \IeC {\h a}nh (encoder)}{27}
\contentsline {subsubsection}{S\IeC {\h \uhorn } d\IeC {\d u}ng CNN (Resnet101)}{28}
\contentsline {subsubsection}{S\IeC {\h \uhorn } d\IeC {\d u}ng Faster-RCNN}{29}
\contentsline {subsection}{\numberline {4.1.2}M\IeC {\^o} h\IeC {\`\i }nh sinh c\IeC {\^a}u m\IeC {\^o} t\IeC {\h a} \IeC {\h a}nh (decoder)}{30}
\contentsline {subsection}{\numberline {4.1.3}Nh\IeC {\d \acircumflex }n x\IeC {\'e}t}{30}
\contentsline {section}{\numberline {4.2}M\IeC {\^o} h\IeC {\`\i }nh \IeC {\dj }\IeC {\`\ecircumflex } xu\IeC {\'\acircumflex }t}{31}
\contentsline {subsection}{\numberline {4.2.1}Thay \IeC {\dj }\IeC {\h \ocircumflex }i m\IeC {\^o} h\IeC {\`\i }nh tr\IeC {\'\i }ch xu\IeC {\'\acircumflex }t \IeC {\dj }\IeC {\d \abreve }c tr\IeC {\uhorn }ng t\IeC {\`\uhorn } \IeC {\h a}nh}{31}
\contentsline {subsection}{\numberline {4.2.2}Thay \IeC {\dj }\IeC {\h \ocircumflex }i m\IeC {\^o} h\IeC {\`\i }nh sinh c\IeC {\^a}u m\IeC {\^o} t\IeC {\h a} \IeC {\h a}nh}{32}
\contentsline {subsection}{\numberline {4.2.3}Nh\IeC {\d \acircumflex }n x\IeC {\'e}t}{33}
\contentsline {chapter}{\numberline {5}Hi\IeC {\d \ecircumflex }n th\IeC {\d \uhorn }c m\IeC {\^o} h\IeC {\`\i }nh}{35}
\contentsline {section}{\numberline {5.1}C\IeC {\^o}ng c\IeC {\d u}}{35}
\contentsline {subsection}{\numberline {5.1.1}Caffe}{35}
\contentsline {subsection}{\numberline {5.1.2}Pytorch}{35}
\contentsline {section}{\numberline {5.2}C\IeC {\^o}ng vi\IeC {\d \ecircumflex }c c\IeC {\`\acircumflex }n chu\IeC {\h \acircumflex }n b\IeC {\d i} cho qu\IeC {\'a} tr\IeC {\`\i }nh hu\IeC {\'\acircumflex }n luy\IeC {\d \ecircumflex }n m\IeC {\^o} h\IeC {\`\i }nh}{36}
\contentsline {subsection}{\numberline {5.2.1}X\IeC {\h \uhorn } l\IeC {\'\i } t\IeC {\d \acircumflex }p d\IeC {\~\uhorn } li\IeC {\d \ecircumflex }u}{36}
\contentsline {subsection}{\numberline {5.2.2}X\IeC {\^a}y d\IeC {\d \uhorn }ng b\IeC {\d \ocircumflex } t\IeC {\`\uhorn } \IeC {\dj }i\IeC {\h \ecircumflex }n}{36}
\contentsline {subsection}{\numberline {5.2.3}Word embedding}{36}
\contentsline {subsection}{\numberline {5.2.4}T\IeC {\h \ocircumflex } ch\IeC {\'\uhorn }c l\IeC {\uhorn }u tr\IeC {\~\uhorn } \IeC {\dj }\IeC {\d \abreve }c tr\IeC {\uhorn }ng cho qu\IeC {\'a} tr\IeC {\`\i }nh hu\IeC {\'\acircumflex }n luy\IeC {\d \ecircumflex }n}{37}
\contentsline {section}{\numberline {5.3}Qu\IeC {\'a} tr\IeC {\`\i }nh h\IeC {\d o}c}{38}
\contentsline {subsection}{\numberline {5.3.1}T\IeC {\`a}i nguy\IeC {\^e}n s\IeC {\h \uhorn } d\IeC {\d u}ng}{38}
\contentsline {subsection}{\numberline {5.3.2}Hu\IeC {\'\acircumflex }n luy\IeC {\d \ecircumflex }n b\IeC {\`\abreve }ng Cross-entropy loss}{38}
\contentsline {subsection}{\numberline {5.3.3}Hu\IeC {\'\acircumflex }n luy\IeC {\d \ecircumflex }n s\IeC {\h \uhorn } d\IeC {\d u}ng h\IeC {\d o}c t\IeC {\u a}ng c\IeC {\uhorn }\IeC {\`\ohorn }ng (Reinforcement Learning)}{39}
\contentsline {subsection}{\numberline {5.3.4}K\IeC {\'\ecircumflex } ho\IeC {\d a}ch hu\IeC {\'\acircumflex }n luy\IeC {\d \ecircumflex }n}{40}
\contentsline {section}{\numberline {5.4}Th\IeC {\'\i } nghi\IeC {\d \ecircumflex }m tr\IeC {\^e}n h\IeC {\d \ecircumflex } s\IeC {\'\ocircumflex } epoch}{41}
\contentsline {chapter}{\numberline {6}\IeC {\DJ }\IeC {\'a}nh gi\IeC {\'a} k\IeC {\'\ecircumflex }t qu\IeC {\h a}}{43}
\contentsline {section}{\numberline {6.1}C\IeC {\'a}c chu\IeC {\h \acircumflex }n \IeC {\dj }o d\IeC {\`u}ng \IeC {\dj }\IeC {\h \ecircumflex } \IeC {\dj }\IeC {\'a}nh gi\IeC {\'a} m\IeC {\^o} h\IeC {\`\i }nh}{43}
\contentsline {subsection}{\numberline {6.1.1}CIDEr: Consensus-based Image Description Evaluation}{43}
\contentsline {subsection}{\numberline {6.1.2}BLEU: Bilingual Evaluation Understudy}{45}
\contentsline {subsection}{\numberline {6.1.3}ROUGE}{45}
\contentsline {section}{\numberline {6.2}K\IeC {\'\ecircumflex }t qu\IeC {\h a} \IeC {\dj }\IeC {\'a}nh gi\IeC {\'a} m\IeC {\^o} h\IeC {\`\i }nh}{46}
\contentsline {subsection}{\numberline {6.2.1}\IeC {\DJ }\IeC {\'a}nh gi\IeC {\'a} c\IeC {\'a}c m\IeC {\^o} h\IeC {\`\i }nh b\IeC {\`\abreve }ng CIDEr, BLEU, ROUGE}{47}
\contentsline {subsection}{\numberline {6.2.2}\IeC {\DJ }i\IeC {\`\ecircumflex }u ch\IeC {\h i}nh c\IeC {\^a}u sinh ra t\IeC {\`\uhorn } m\IeC {\^o} h\IeC {\`\i }nh}{61}
\contentsline {section}{\numberline {6.3}Th\IeC {\h a}o lu\IeC {\d \acircumflex }n}{63}
\contentsline {chapter}{\numberline {7}T\IeC {\h \ocircumflex }ng k\IeC {\'\ecircumflex }t}{65}
